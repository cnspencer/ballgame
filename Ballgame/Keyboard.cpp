#include "StdAfx.h"
#include "Keyboard.h"

Keyboard::Keyboard(HINSTANCE hInst, HWND hWnd, LPDIRECTINPUT8 din) :
	hInstance(hInst),
	hWindow(hWnd),
	dinkeyboard(NULL)
{
    // create the keyboard device
    din->CreateDevice(GUID_SysKeyboard,    // the default keyboard ID being used
                      &dinkeyboard,    // the pointer to the device interface
                      NULL); 

    dinkeyboard->SetDataFormat(&c_dfDIKeyboard); // set the data format to keyboard format

    // set the control you will have over the keyboard
    dinkeyboard->SetCooperativeLevel(hWindow,
                                     DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);

	ZeroMemory(&keyState, sizeof(keyState));
}

Keyboard::~Keyboard(void)
{
	if(dinkeyboard)
	{
		dinkeyboard->Unacquire();    // make sure the keyboard is unacquired
		dinkeyboard->Release();
	}
	dinkeyboard = NULL;
}

// this is the function that detects keystrokes and displays them in a message box
void Keyboard::DetectKeys(void)
{
    dinkeyboard->Acquire();    // get access if we don't have it already

    dinkeyboard->GetDeviceState(256, (LPVOID)keyState);    // fill keystate with values

}

bool Keyboard::IsKeyPressed(int dik_code)
{
    dinkeyboard->Acquire();    // get access if we don't have it already
    dinkeyboard->GetDeviceState(256, (LPVOID)keyState);    // fill keystate with values

	return keyState[dik_code] & 0x80 ? true : false;
}