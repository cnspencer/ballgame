#pragma once
//#include "Edge.h"
//#include "Ball.h"
#include <map>

class Edge;
class Ball;

//the index into the collision id tables
typedef int collision_id;

class CollisionManager
{
public:
	CollisionManager(void);
	~CollisionManager(void);

	//returns your id
	//automatically calls SetCollisionID on the object
	collision_id AddEdge(Edge &edge);
	collision_id AddBall(Ball &ball);

	void RemoveEdge(Edge &edge);
	void RemoveBall(Ball &ball);

	//checks to see if the 2 objects are colliding, and if they are
	bool CheckCollisionPointBall(D3DXVECTOR2 &point, Ball &ball, const float &dt);
	bool CheckCollisionEdgeBall(collision_id edge_id, collision_id ball_id, const float &dt);
	bool CheckCollisionTwoBalls(collision_id ball1, collision_id ball2, const float &dt);

	//checks for all collisions
	void CheckAllCollisions(const float &dt);
private:
	std::map<collision_id, Edge*> edges;
	std::map<collision_id, Ball*> balls;

	//the next id to give
	collision_id new_id;

	//call this method once we have found a collision
	void FoundCollision(Ball &ball1, Ball &ball2, float squared_distance, const float &dt);
	void FoundCollision(Edge &edge, Ball &ball, D3DXVECTOR2 &closest_point, const float &dt);
	void FoundCollision(D3DXVECTOR2 &point, Ball &ball, const float &dt);
};
