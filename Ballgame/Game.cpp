#include "StdAfx.h"
#include "Game.h"
#include "Graphics.h"
#include "Input.h"
#include "Keyboard.h"
#include "Mouse.h"

//the global Game object
extern Game *game_ptr;

Game::Game(HINSTANCE hInst, HWND hWnd) :
	graphics(NULL),
	hInstance(hInst),
	hWindow(hWnd),
	input(NULL)
{
	graphics = new Graphics(hWnd);
	input = new Input(hInst, hWnd, this);
}

Game::~Game(void)
{
	if(input)
		delete input;
	input = NULL;

	if(graphics)
		delete graphics;
	graphics = NULL;
}

bool Game::MainLoop(void)
{
	//time that the last loop began
	clock_t lastBeginTime = clock();

	// Main message loop:
	for(;;)
	{
		// See if the window is being destroyed
		if(!IsWindow(hWindow))
			break;

		//------set up dt, the time since last frame-------------------------

		//this time will be used throughout the loop
		//this is the only point where we call clock() in the loop
		clock_t beginTime = clock();
		clock_t differenceInTicks = beginTime - lastBeginTime;

		//just go until we get some difference
		if(differenceInTicks <= 0)
		{
			continue;
		}

		lastBeginTime = beginTime;
		float dt;//the time difference

		//calculate the number of seconds since last iteration
		//this will definitely be a fraction less than 1 (a float)
		//CLOCKS_PER_SEC is just a macro, so we don't have to type cast (its treated as an int, not long)
		dt = static_cast<float>(differenceInTicks) / CLOCKS_PER_SEC;

		//--------handle input--------------------------------------------

		//if it returns true, it's time to end the game
		if(input->CheckInput(dt))
			break;

		//--------update--------------------------------------------
		graphics->UpdateFrame(dt);

		//--------draw--------------------------------------------
		if(graphics->RenderFrame(dt))
			break;
	}

	return 0;
}