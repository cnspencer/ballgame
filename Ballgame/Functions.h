#pragma once

#include "Stdafx.h"


namespace bgfunc
{
	inline D3DXCOLOR RandomColorXRGB()
	{
		D3DCOLOR color;
		DWORD red = rand() % 256;
		DWORD green = rand() % 256;
		DWORD blue = rand() % 256;
		color = 0xff000000 | (red << 16) | (green << 8) | blue;
		return color;
	}

	
	inline D3DXVECTOR2 ProjectVector(D3DXVECTOR2 &source, D3DXVECTOR2 &direction)
	{
		float square_length = direction.x * direction.x + direction.y * direction.y;
		D3DXVECTOR2 proj(direction);
		proj *= (source.x * direction.x + source.y * direction.y) / square_length;
		return proj;
	}

	inline float DotProduct(D3DXVECTOR2 &vec1, D3DXVECTOR2 &vec2)
	{
		return (vec1.x * vec2.x) + (vec1.y * vec2.y);
	}
}
