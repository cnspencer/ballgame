#include "StdAfx.h"
#include "Ball.h"
#include <sstream>

Ball::Ball(D3DXVECTOR2 &center, float radius, LPDIRECT3DDEVICE9 d3ddev, D3DCOLOR &color, float mass,
		   int sections, D3DXVECTOR2 &velocity) :
	Drawable(d3ddev),
	points(NULL),
	center(center),
	radius(radius),
	numPoints(sections + 2),
	velocity(velocity),
	color(color),
	collision_flag(false),
	collision_point(0.0f, 0.0f),
	col_id(-1),
	mass(mass),
	proposedCenter(),
	proposedCenterSet(false)
{


	points = new CUSTOMVERTEX[numPoints];
	pointsTw = new CUSTOMVERTEX[numPoints];

	//first point occurs to the right of the center
	points[0].x = 0.0f + center.x;
	points[0].y = 0.0f + center.y;
	points[0].z = 0.5f;
	points[0].rhw = 1.0f;
	points[0].color = color;

	points[1].x = radius + center.x;
	points[1].y = 0.0f + center.y;
	points[1].z = 0.5f;
	points[1].rhw = 1.0f;
	points[1].color = color;

	//iterate around with this angle to initialize points
	float current_angle = 0.0;//in radians
	float pi = PI;//wont compile unless i separate, dont know why
	float angle_increment = (2.0F * pi)/sections;

	//initialize the rest of the points
	for(int i = 2; i < numPoints; ++i)
	{
		current_angle += angle_increment;

		points[i].x = radius * cos(current_angle) + center.x;
		points[i].y = radius * sin(current_angle) + center.y;
		points[i].z = 0.5f;
		points[i].rhw = 1.0f;
		points[i].color = color;
	}

	//we must create the vertex buffer
	if(FAILED(d3ddev->CreateVertexBuffer(numPoints * sizeof(CUSTOMVERTEX),
		0,
		D3DFVF_CUSTOMVERTEX,
		D3DPOOL_MANAGED,
		&pVertexBuffer,
		NULL)))
	{
		throw std::exception("Ball: failed to init vertex buffer");
	}

	/////now do the whole thing all over again for the second circle
	//first copy the points over
	memcpy(pointsTw, points, sizeof(CUSTOMVERTEX) * numPoints);
	//adjust color to have 1/2 alpha
	for(int i = 0; i < numPoints; ++i)
		pointsTw[i].color &= 0x7fffffff;//set alpha to half (its like a ghost that follows behind the ball)

	//we must create the vertex buffer
	if(FAILED(d3ddev->CreateVertexBuffer(numPoints * sizeof(CUSTOMVERTEX),
		0,
		D3DFVF_CUSTOMVERTEX,
		D3DPOOL_MANAGED,
		&pVertexBufferTw,
		NULL)))
	{
		throw std::exception("Ball: failed to init vertex buffer for tweening");
	}

	RefreshVB();

}

Ball::~Ball(void)
{
	delete[] pointsTw;
	delete[] points;

	if(pVertexBuffer)
		pVertexBuffer->Release();
	pVertexBuffer = NULL;

	if(pVertexBufferTw)
		pVertexBufferTw->Release();
	pVertexBufferTw = NULL;
}

//only moves the points, not the center
void Ball::Move(float dx, float dy, const float &dt)
{
	old_center = center;
	center = GetProposedCenter(dt);

	//put center values where the old center stuff goes
	for(int i = 0; i < numPoints; ++i)
	{
		pointsTw[i].x = points[i].x;
		pointsTw[i].y = points[i].y;
	}

	for(int i = 0; i < numPoints; ++i)
	{
		points[i].x += dx;
		points[i].y += dy;
	}
	RefreshVB();
}

//here just move the center forward for collision detection
void Ball::PreCollision(const float &dt)
{
//	center.x += velocity.x*dt;
//	center.y += velocity.y*dt;
}

//move all the points forward either by velocity, or in a special way if collision occurred
void Ball::PostCollision(const float &dt)
{

	//sets the center to what it should become according to the velocity
//	center = GetProposedCenter(dt);

	//move along as per velocity
	float dx = velocity.x * dt;
	float dy = velocity.y * dt;
	Move(dx, dy, dt);
}

//copies the vecotor *points into the vertex buffer
//also refreshes the buffer for tweening
void Ball::RefreshVB()
{
	//now load the vertex buffer
	VOID* pTempBuffer;

	//the regular buffer
	if(FAILED(pVertexBuffer->Lock(0, 0, (void**)&pTempBuffer, 0)))
	{
		throw std::exception("Ball: failed to lock vertex buffer");
	}
	memcpy(pTempBuffer, points, sizeof(CUSTOMVERTEX) * numPoints);
	if(FAILED(pVertexBuffer->Unlock()))
		throw std::exception("Edge: failed to unlock vertex buffer");

	//handle the tweening vb
	if(FAILED(pVertexBufferTw->Lock(0, 0, (void**)&pTempBuffer, 0)))
	{
		throw std::exception("Ball: failed to lock vertex buffer tw");
	}
	memcpy(pTempBuffer, pointsTw, sizeof(CUSTOMVERTEX) * numPoints);
	if(FAILED(pVertexBufferTw->Unlock()))
		throw std::exception("Edge: failed to unlock vertex buffer tw");
}

void Ball::Update(const float &dt)
{
	ApplyGravity(dt);
	ApplyDrag(dt);
}

void const Ball::Draw(const float &dt)
{
	//draw the tweening behind
	d3ddev->SetStreamSource(0, pVertexBufferTw, 0, sizeof(CUSTOMVERTEX));
	d3ddev->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, numPoints - 2);

	//draw the "actual" circle
	d3ddev->SetStreamSource(0, pVertexBuffer, 0, sizeof(CUSTOMVERTEX));
	d3ddev->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, numPoints - 2);
}



void Ball::ApplyDrag(const float &dt)
{
	

	//adjust velocity
	//lower it by 50 pixels / sec^2
	float drag = 50.0f*dt;

	if(velocity.x != 0.0f || velocity.y != 0.0f)
	{
		float length = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y));
		float dragx = (velocity.x * drag) / (length);
		float dragy = (velocity.y * drag) / (length);

		if(velocity.x > 0.0f)
			velocity.x = velocity.x < dragx ? 0.0f : velocity.x - dragx;
		else if (velocity.x < 0.0f)
			velocity.x = velocity.x > dragx ? 0.0f : velocity.x - dragx;

		if(velocity.y > 0.0f)
			velocity.y = velocity.y < dragy ? 0.0f : velocity.y - dragy;
		else if (velocity.y < 0.0f)
			velocity.y = velocity.y > dragy ? 0.0f : velocity.y - dragy;
	}
}


void Ball::ApplyGravity(const float &dt)
{
	//1000 pixels / sec^2
	velocity.y += 1000.0f * dt;
}