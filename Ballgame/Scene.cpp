#include "StdAfx.h"
#include "Scene.h"
#include "Edge.h"
#include "Ball.h"
#include <sstream>
#include <typeinfo>


Scene::Scene(LPDIRECT3DDEVICE9 d3ddev) :
	Drawable(d3ddev),
	d3ddev(d3ddev),
	drawn_objects(),
	next_id(0),
	collisionManager()
{
}

Scene::~Scene(void)
{
	//all class members are "local" so no deletes necessary, except for the classes held in the maps
	
	//may not be the fastest, but oh well
	std::map<int, Drawable*>::iterator del_iterator;

	for(
		del_iterator = drawn_objects.begin(); 
		del_iterator != drawn_objects.end(); 
		++del_iterator)
	{
		Drawable *drawable = del_iterator->second;
		delete drawable;
	}

	drawn_objects.erase(drawn_objects.begin(), drawn_objects.end());
}

void Scene::Update(const float &dt)
{
	//may not be the fastest, but oh well
	std::map<int, Drawable*>::iterator update_iterator;

	for(
		update_iterator = drawn_objects.begin(); 
		update_iterator != drawn_objects.end(); 
		++update_iterator)
	{
		update_iterator->second->Update(dt);
	}

	CullOffscreenBalls(dt);
}

void const Scene::Draw(const float &dt)
{
	//may not be the fastest, but oh well
	std::map<int, Drawable*>::iterator draw_iterator;

	for(
		draw_iterator = drawn_objects.begin(); 
		draw_iterator != drawn_objects.end(); 
		++draw_iterator)
	{
		draw_iterator->second->Draw(dt);
	}

}


void Scene::CullOffscreenBalls(const float &dt)
{
	//int count = 0;
	D3DCOLOR clr = 0;

	std::map<int, Drawable*>::iterator cull_iterator = drawn_objects.begin();//, 
		//end_it = drawn_objects.end();

	for(;cull_iterator !=  drawn_objects.end(); /*++cull_iterator*/)
	{
		//std::map<int, Drawable*>::iterator new_iterator = cull_iterator;

		Drawable *drawable = cull_iterator->second;
		Ball *ball;

		//if( strcmp(typeid(drawable).name(), "Ball"))
		if(typeid(*drawable) == typeid(Ball))
		{
			ball = dynamic_cast<Ball *>(cull_iterator->second);
		}
		else
		{
			++cull_iterator;
		//	++count;
			continue;
		}

		D3DXVECTOR2 center = ball->GetCenter();
		float radius = ball->GetRadius();

		

//		if(center.x < 0.0f - radius || center.x > GetSystemMetrics(SM_CXSCREEN) + radius ||
//			center.y < 0.0f - radius || center.y > GetSystemMetrics(SM_CYSCREEN) + radius)
		if(center.y - radius > GetSystemMetrics(SM_CYSCREEN))
		{
			//here we delete it
			drawn_objects.erase(cull_iterator++);//ball->GetSceneID());
			collisionManager.RemoveBall(*ball);

			// now we may actually destroy the ball forever
			delete ball;
		}
		else
			++cull_iterator;

		//++count;

	}


}



void Scene::PreCollision(const float &dt)
{
	//may not be the fastest, but oh well
	std::map<int, Drawable*>::iterator precol_iterator;

	for(
		precol_iterator = drawn_objects.begin(); 
		precol_iterator != drawn_objects.end(); 
		++precol_iterator)
	{
		precol_iterator->second->PreCollision(dt);
	}
}

void Scene::CheckCollisions(const float &dt)
{
	collisionManager.CheckAllCollisions(dt);
}

void Scene::PostCollision(const float &dt)
{
	//this is now handled by collision manager
	/*
	//may not be the fastest, but oh well
	std::map<int, Drawable*>::iterator postcol_iterator;

	for(
		postcol_iterator = drawn_objects.begin(); 
		postcol_iterator != drawn_objects.end(); 
		++postcol_iterator)
	{
		postcol_iterator->second->PostCollision(dt);
	}*/
}