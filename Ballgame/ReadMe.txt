Readme.txt for Ballgame program


This program is basically just a full-screen application for Windows which lets the user place balls and line segments on the screen.  They interact physically with collision detection, and gravity and drag both affect the movement of the balls.

To place a ball on the screen, click the left mouse button.

To place a line, click the right mouse button with the cursor at separate points for each click.  The line segment will go between those two points.

To exit the program, press the Escape key.