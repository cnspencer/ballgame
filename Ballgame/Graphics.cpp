#include "StdAfx.h"
#include "Graphics.h"
#include "Scene.h"


Graphics::Graphics(HWND hWnd) :
	d3d(NULL),
	d3ddev(NULL),
	scene(NULL),
	backgroundColor(0xff000000),
	red(0.0f),
	green(255.0f),
	blue(255.0f),
	font(NULL)
{
	InitD3D(hWnd);	

	//create the scene
	try
	{
		scene = new Scene(d3ddev);
	}
	catch (...)
	{
		CleanD3D();
		throw;
	}
}

Graphics::~Graphics(void)
{
	if(scene)
		delete scene;
	scene = NULL;

	CleanD3D();
}

//private copy constructor


void Graphics::InitD3D(HWND hWnd)// sets up and initializes Direct3D
{
    d3d = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface


    ZeroMemory(&d3dpp, sizeof(d3dpp));    // clear out the struct for use
    d3dpp.Windowed = FALSE;//TRUE;    // fullscreen
    d3dpp.SwapEffect = D3DSWAPEFFECT_FLIP;    // page flipping
    d3dpp.hDeviceWindow = hWnd;    // set the window to be used by Direct3D
    d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;    // set the back buffer format to 32-bit with alpha
    d3dpp.BackBufferWidth = GetSystemMetrics(SM_CXSCREEN);///2;    // set the width of the buffer
    d3dpp.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);///2;    // set the height of the buffer


    // create a device class using this information and the info from the d3dpp stuct
	if(FAILED(d3d->CreateDevice(D3DADAPTER_DEFAULT,
                      D3DDEVTYPE_HAL,
                      hWnd,
                      D3DCREATE_HARDWARE_VERTEXPROCESSING,
                      &d3dpp,
                      &d3ddev)))
	{
		//its a constructor, so we must throw an exception
		CleanD3D();
		throw std::exception("Graphics: could not create D3D device");
	}
	d3ddev->SetRenderState( D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1 );

	if(FAILED(d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE)))
		throw std::exception("failed to allow alpha blending");
	d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
	d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA  );



	// for fun I'm going to make this for text
//	HRESULT hRes = D3DXCreateFont(d3ddev, 20, 0, 0, 0, false, DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, DEFAULT_QUALITY, 
//		DEFAULT_PITCH | FF_DONTCARE, TEXT("Arial"), &font);


	if(FAILED(D3DXCreateFont(d3ddev, 32, 0, 0, 0, false, DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, DEFAULT_QUALITY, 
		DEFAULT_PITCH | FF_DONTCARE, L"andalus", &font)))
	{
		throw std::exception("look in Graphics... failed to create font");
	}
//	char buffer[50];
//	sprintf(buffer, "%d", (int)font);
//	throw std::exception(buffer);
}
void Graphics::UpdateFrame(const float &dt)  //updates a single frame
{
	//update the background color
	red += 0.0145f;
	green += 0.0212f;
	blue += 0.042351f;

	backgroundColor.r = 0.5f * (sin(red) + 1.0f);
	backgroundColor.g = 0.5f * (sin(green) + 1.0f);
	backgroundColor.b = 0.5f * (sin(blue) + 1.0f);


	//first do collision detection functions
	scene->PreCollision(dt);
	scene->CheckCollisions(dt);
	scene->PostCollision(dt);

	//generic update
	scene->Update(dt);
}

bool Graphics::RenderFrame(const float &dt) const  // renders a single frame
{
	//test if the device is ok
	if(FAILED(d3ddev->TestCooperativeLevel()))
	{
		throw std::exception("Test cooperative level failed for D3D device");
		//return true;//error occurred
	}

    // clear the window to the changing background color
    d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, backgroundColor/*D3DCOLOR_XRGB(0, 40, 100)*/, 1.0f, 0);

	if(SUCCEEDED(d3ddev->BeginScene())){   // begins the 3D scene

        // select which vertex format we are using
        d3ddev->SetFVF(D3DFVF_CUSTOMVERTEX);

		//draw the scene
		scene->Draw(dt);

		RECT fontRect;
		fontRect.left = 10;
		fontRect.right = 500;
		fontRect.bottom = 300;
		fontRect.top = 10;

		// for fun I will render some text
		font->DrawTextA(NULL, "Left mouse: place ball\nRight mouse (2 clicks): place line\nPress Esc to exit.", 
			-1, &fontRect, DT_LEFT | DT_TOP | DT_WORDBREAK, 
			D3DXCOLOR(1.0f - backgroundColor.r,
			1.0f - backgroundColor.g,
			1.0f - backgroundColor.b,
			1.0f)
			);
	
		d3ddev->EndScene();    // ends the 3D scene
	}

    d3ddev->Present(NULL, NULL, NULL, NULL);   // displays the created frame on the screen


	return false;//everything went ok
}

void Graphics::CleanD3D(void)    // closes Direct3D and releases memory
{
	if(d3ddev)
		d3ddev->Release();    // close and release the 3D device
	d3ddev = NULL;

	if(d3d)
		d3d->Release();    // close and release Direct3D
	d3d = NULL;

	ZeroMemory(&d3dpp, sizeof(d3dpp));
}