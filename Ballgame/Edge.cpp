#include "StdAfx.h"
#include "Edge.h"


Edge::Edge(float x0, float y0, float x1, float y1, D3DCOLOR clr, LPDIRECT3DDEVICE9 d3ddev, int vertbufid) :
	Drawable(d3ddev),
	color(clr),
	pVertexBuffer(NULL),
	vertbufid(vertbufid),
	col_id(-1)
{
	//fill the points
	points[0].x = x0;
	points[0].y = y0;
	points[0].z = 0.5f;
	points[0].rhw = 1.0f;
	points[0].color = clr;

	points[1].x = x1;
	points[1].y = y1;
	points[1].z = 0.5f;
	points[1].rhw = 1.0f;
	points[1].color = clr;


	//we must create the vertex buffer
	if(FAILED(d3ddev->CreateVertexBuffer(2 * sizeof(CUSTOMVERTEX),
		0,
		D3DFVF_CUSTOMVERTEX,
		D3DPOOL_MANAGED,
		&pVertexBuffer,
		NULL)))
	{
		throw std::exception("Edge: failed to init vertex buffer");
	}

	//now load the vertex buffer
	VOID* pTempBuffer;

	if(FAILED(pVertexBuffer->Lock(0, 0, (void**)&pTempBuffer, 0)))
	{
		throw std::exception("Edge: failed to lock vertex buffer");
	}
	memcpy(pTempBuffer, points, sizeof(points));
	if(FAILED(pVertexBuffer->Unlock()))
		throw std::exception("Edge: failed to unlock vertex buffer");
}

Edge::~Edge(void)
{
	if(pVertexBuffer)
		pVertexBuffer->Release();
	pVertexBuffer = NULL;
}

void Edge::Update(const float &dt)
{
	//we dont really need to update a line
}

void const Edge::Draw(const float &dt)
{

	d3ddev->SetStreamSource(0, pVertexBuffer, 0, sizeof(CUSTOMVERTEX));
	d3ddev->DrawPrimitive(D3DPT_LINELIST, 0, 1);
}

//the edge doesnt move so just do nothing here
void Edge::PreCollision(const float &dt)
{}
void Edge::PostCollision(const float &dt)
{}