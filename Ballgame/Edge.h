#pragma once
#include "Drawable.h"
#include <D3D9Types.h>
#include "Graphics.h"
#include "CollisionManager.h"

class Drawable;

//these lines are necessary to set the error
//extern void SetError(std::wstring &error_string);
//extern void SetError(wchar_t *new_error);

class Edge : public Drawable
{
public:
	Edge(float x0, float y0, float x1, float y1, D3DCOLOR clr, LPDIRECT3DDEVICE9 d3ddev, int vertbufid);
	virtual ~Edge(void);

	//from Drawable
	virtual void Update(const float &dt);
	virtual void const Draw(const float &dt);

	virtual void PreCollision(const float &dt);
	virtual void PostCollision(const float &dt);

	void SetCollisionID(collision_id new_id){col_id = new_id;}
	collision_id GetCollisionID(){return col_id;}

	const D3DXVECTOR2 GetPoint(int index)
	{
		D3DXVECTOR2 vec(points[index].x, points[index].y);
		return vec;
	}
private:
	int vertbufid;//the id of the vertex buffer
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer;

	CUSTOMVERTEX points[2];//endpoints of the line
	
	collision_id col_id;//the collision id

	D3DCOLOR color;//the color of the line
};
