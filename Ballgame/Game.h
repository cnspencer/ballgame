#pragma once


//forward declarations
class Graphics;
class Input;
class Keyboard;
class Mouse;

class Game
{
public:
	Game(HINSTANCE hInst, HWND hWnd);
	~Game(void);

	//passes control to the game's main loop
	//returns the error_flag when its finished
	bool MainLoop(void);

	Graphics* GetGraphics(){return graphics;}

//	RECT GetScreenRect() { return Get

private:
	//so that we don't accidentally copy it
	//also the body is not defined, so we won't accidentally call it inside the class either
	//(or else we would get a linker error)
	Game(const Game& game);

	//I use reference variables here for efficiency
	Graphics *graphics;
	Input *input;					// all the input
	HINSTANCE hInstance;			// current instance
	HWND hWindow;					// the game window
};