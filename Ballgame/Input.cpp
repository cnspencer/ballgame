#include "StdAfx.h"
#include "Input.h"
#include "Keyboard.h"
#include "Mouse.h"

Input::Input(HINSTANCE hInst, HWND hWnd, Game *game):
	hInstance(hInst),
	hWindow(hWnd),
	din(NULL),
	keyboard(NULL),
	mouse(NULL),
	game(game)
{
    // create the DirectInput interface
    DirectInput8Create(hInst,    // the handle to the application
                       DIRECTINPUT_VERSION,    // the compatible version
                       IID_IDirectInput8,    // the DirectInput interface version
                       (void**)&din,    // the pointer to the interface
                       NULL);    // COM stuff, so we'll set it to NULL

	keyboard = new Keyboard(hInst, hWnd, din);
	mouse = new Mouse(hInst, hWnd, din, game);
}

Input::~Input(void)
{
	if(keyboard)
		delete keyboard;
	keyboard = NULL;

	if(mouse)
		delete mouse;
	mouse = NULL;

	if(din)
		din->Release();    // close DirectInput before exiting
	din = NULL;
}


//handles any input
bool Input::CheckInput(const float &dt)
{
	keyboard->DetectKeys();
	if(keyboard->IsKeyPressed(DIK_ESCAPE)){
		return true;
	}

	mouse->CheckForClick(dt);
	return false;
}
