#include "StdAfx.h"
#include "Mouse.h"
#include "Graphics.h"
#include "Ball.h"
#include "Edge.h"
#include "Game.h"
#include "Scene.h"
#include "Functions.h"

Mouse::Mouse(HINSTANCE hInst, HWND hWnd, LPDIRECTINPUT8 din, Game *game) :
	hInstance(hInst),
	hWindow(hWnd),
	dinMouse(NULL),
	game(game),
	mouseState(NORMAL),
	velocity_start(0.0f, 0.0f),
	edge_start(0.0f, 0.0f),
	ball(NULL),
	edge(NULL)
{
	velocity_start.x = 0.0f;

	if(FAILED(din->CreateDevice(GUID_SysMouseEm, &dinMouse, NULL)))
	{
		throw std::exception("failed to create mouse device");
	}

	if(FAILED(dinMouse->SetDataFormat(&c_dfDIMouse)))
		throw std::exception("failed to set data format for mouse");

    // set the control you will have over the mouse
	if(FAILED(dinMouse->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND)))
		throw std::exception("failed to set cooperative level on mouse");

	//set buffer size for mouse input
	//...
	DIPROPDWORD inputBufferSizeDword;
	inputBufferSizeDword.dwData = 6 * sizeof(DIDEVICEOBJECTDATA); //* sizeof(DIDEVICEOBJECTDATA);//here we actually have the buffer size

	inputBufferSizeDword.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	inputBufferSizeDword.diph.dwSize = sizeof(DIPROPDWORD);
	inputBufferSizeDword.diph.dwHow = DIPH_DEVICE;
	inputBufferSizeDword.diph.dwObj = 0;


//	inputBufferSize
	if(FAILED(dinMouse->SetProperty(DIPROP_BUFFERSIZE, &inputBufferSizeDword.diph)))
		throw std::exception("failed to set buffer size for mouse input");

	ZeroMemory(&objData, sizeof(objData));

}

Mouse::~Mouse(void)
{
	if(dinMouse)
	{
		dinMouse->Unacquire();
		dinMouse->Release();
	}
	dinMouse = NULL;
}

//see if the mouse has been clicked and if so, call InterpretClick
void Mouse::CheckForClick(const float &dt)
{
	//acquire the device
	if(FAILED(dinMouse->Acquire()))
	{
		//just return... better luck next time
		return;
		//throw std::exception("failed to acquire mouse");
	}

	//grab the data
	//elements will get changed to the amount actually read
	DWORD elements = 6;
	if(FAILED(dinMouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),
		objData,
		&elements,
		0)))
	{
		//here we probably have a critical error
		throw std::exception("failed to get mouse device data");
	}

	//iterate through the elements
	for(unsigned int i = 0; i < elements; ++i)
	{

		//I'm going to use windows instead of DirectInput to get the cursor position
		//because DirectInput only gets the relative position		
		POINT cursorpos;

		if(objData[i].dwOfs == DIMOFS_BUTTON0 && //left button
			objData[i].dwData == 0x80)
		{	
			GetCursorPos(&cursorpos);
			InterpretClick(true, cursorpos.x, cursorpos.y, dt);
		}
		else if(objData[i].dwOfs == DIMOFS_BUTTON1 && //right button
			objData[i].dwData == 0x80)
		{
			GetCursorPos(&cursorpos);
			InterpretClick(false, cursorpos.x, cursorpos.y, dt);
		}
	}
}


//checks the mouse click and takes an action in the game
//if its not the left button, its the right button
void Mouse::InterpretClick(bool isLeftButton, long x, long y, const float &dt)
{
	//	throw std::exception("botton pressed");
	switch(mouseState)
		case NORMAL: 
		//the regular case
		{
			{

			if(isLeftButton)
			{
				//when the left mouse button is hit, we make a ball at the cursor position
				ball = CreateBall(static_cast<float>(x), static_cast<float>(y));

				// add it to the scene and set it up for collision detection
				int scene_id = game->GetGraphics()->getScene()->add(ball);
				ball->SetSceneID(scene_id);
				collision_id col_id = game->GetGraphics()->getScene()->GetCollisionManager().AddBall(*ball);
				ball->SetCollisionID(col_id); // not strictly necessary

				//later we will allow the velocity to get set with a second click
				//set the state so velocity can be set by a subsequent click
				//also record the current point
				velocity_start.x = static_cast<float>(x);
				velocity_start.y = static_cast<float>(y);

				//I switched to single-click ball placement, hence the comment
//				mouseState = BALL_STARTED;
			}
			else //right button
			{
				edge_start.x = static_cast<float>(x);
				edge_start.y = static_cast<float>(y);

				mouseState = EDGE_STARTED;
			}
		} break; // end case NORMAL

		//this case is not currently used, but if you remove the comment above that says mouseState = BALL_STARTED
		//you will have to click twice to place a ball, but the second click gives it velocity
		case BALL_STARTED:
		{
			if(ball != NULL)
			{
				D3DXVECTOR2 new_vel = -(ball->GetCenter());
				new_vel.x += static_cast<float>(x);
				new_vel.y += static_cast<float>(y);
				ball->SetVelocity(new_vel);
			}
			ball = NULL;
			mouseState = NORMAL;
		} break; // end case BALL_STARTED
		case EDGE_STARTED:
		//here, any button will create the line
		{
			//generate a random color
			D3DCOLOR color = bgfunc::RandomColorXRGB();

			//create the edge
			edge = new Edge(edge_start.x, edge_start.y, static_cast<float>(x), static_cast<float>(y),
				color, game->GetGraphics()->get_d3ddev(), 0);

			//register the edge
			game->GetGraphics()->getScene()->add(edge);
			game->GetGraphics()->getScene()->GetCollisionManager().AddEdge(*edge);

			edge = NULL;
			mouseState = NORMAL;

		} break; // end case EDGE_STARTED
	}//end switch
}

//randomly generates a ball
Ball* Mouse::CreateBall(float x, float y)
{
	D3DCOLOR color = bgfunc::RandomColorXRGB();

	//generate a random radius
	int radius = (rand() % 10) + 10;
	//int radius = rand() % 2 == 0 ? 50 : 10;


	float mass = static_cast<float>(radius * radius * (PI));

	Ball *ball;
	ball = new Ball(D3DXVECTOR2(x, y),
		static_cast<float>(radius), game->GetGraphics()->get_d3ddev(), color, mass, radius, D3DXVECTOR2(0.0f, 0.0f));

	return ball;
}