#pragma once

#include "Drawable.h"
#include "Graphics.h"
#include "CollisionManager.h"


class Ball : public Drawable
{
public:
	Ball(D3DXVECTOR2 &center, float radius, LPDIRECT3DDEVICE9 d3ddev, D3DCOLOR &color, float mass,
		int sections = 12, D3DXVECTOR2 &velocity = D3DXVECTOR2(0.0, 0.0));
	virtual ~Ball(void);

	//tells the ball to move this amount
	//positive y direction is downward
	virtual void Move(float dx, float dy, const float &dt);

	//copies the vecotor *points into the vertex buffer
	virtual void RefreshVB();

	virtual void SetVelocity(D3DXVECTOR2 &v)
	{	velocity = v;  }
	virtual D3DXVECTOR2 GetVelocity(){return velocity;}
	
	virtual void Update(const float &dt);
	virtual void const Draw(const float &dt);

	const D3DXVECTOR2& GetCenter()
	{	return center;  }
	void SetCenter(D3DXVECTOR2 & newCenter)
	{	center = newCenter;  }
	
	//gives the center after the next tick (really just center + velocity)
	D3DXVECTOR2 GetProposedCenter(const float &dt)
	{	
		D3DXVECTOR2 ret(center.x, center.y);
		ret.x += velocity.x * dt;
		ret.y += velocity.y * dt;
		return ret;  
	}

	virtual void PreCollision(const float &dt);
	virtual void PostCollision(const float &dt);


	void SetCollisionID(collision_id new_id){col_id = new_id;}
	collision_id GetCollisionID(){return col_id;}
	float GetRadius(){return radius;}
	float GetMass(){return mass;}

	int GetSceneID() { return scene_id; }
	void SetSceneID(int new_id) { scene_id = new_id; }

private:
	//these are the points used to draw the ball
	CUSTOMVERTEX *points;
	int numPoints;
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer;
	int vbnum;//the vertex buffer number

	//points to the center from upper left
	D3DXVECTOR2 center;
	float radius;
	
	//the new center for this frame
	D3DXVECTOR2 proposedCenter;
	bool proposedCenterSet;

	//the old values are used for tweening
	D3DXVECTOR2 old_center;
	CUSTOMVERTEX *pointsTw;
	LPDIRECT3DVERTEXBUFFER9 pVertexBufferTw;
	
	//the ball's color
	D3DCOLOR color;

	//velocity is given in pixels/second
	D3DXVECTOR2 velocity;
	
	//collision flag, gets set if a collision occurred
	bool collision_flag;
	D3DXVECTOR2 collision_point;
	collision_id col_id;//the collision id

	// the scene id
	int scene_id;

	//the mass of the ball
	float mass;

	//offset time since the beginning of the frame when this collision occurred
//	float collision_offset_time;

	//applies some drag for this iteration
	//all it does is reduce the velocity vector
	void ApplyDrag(const float &dt);

	//applies gravity
	void ApplyGravity(const float &dt);
};
