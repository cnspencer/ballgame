#pragma once

//class Graphics;

/*abstract class*/
class Drawable
{
protected:
	LPDIRECT3DDEVICE9 d3ddev;    // the pointer to the device class

public:
	Drawable(LPDIRECT3DDEVICE9 d3ddev) :
		d3ddev(d3ddev)
	{}

	virtual ~Drawable(void);

	virtual void Update(const float &dt) = 0;
	virtual void const Draw(const float &dt) = 0;

	virtual void PreCollision(const float &dt) = 0;
	virtual void PostCollision(const float &dt) = 0;
};
