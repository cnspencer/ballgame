#pragma once

class Keyboard
{
private:
	LPDIRECTINPUTDEVICE8 dinkeyboard;    // the pointer to the keyboard device
	HINSTANCE hInstance;
	HWND hWindow;
	BYTE keyState[256];
public:
	Keyboard(HINSTANCE hInst, HWND hWnd, LPDIRECTINPUT8 din);
	~Keyboard(void);

	bool KeyPressed();
	void DetectKeys();
	bool IsKeyPressed(int dik_code);
};
