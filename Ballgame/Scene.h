#pragma once

#include "Drawable.h"
//#include <vector>
#include "CollisionManager.h"
#include <map>

class Scene : public Drawable
{
public:
	Scene(LPDIRECT3DDEVICE9 d3ddev);
	~Scene(void);

	//this adds a new object to the scene and returns the identifier
	int add(Drawable* drawable)
	{
		int new_id = next_id++;
		drawn_objects.insert(std::pair<int, Drawable*>(new_id, drawable));
		return new_id;
	}

	CollisionManager& GetCollisionManager(){return collisionManager;}


//	void remove(int){};


	//from Drawable
	void Update(const float &dt);
	void const Draw (const float &dt);
	void CullOffscreenBalls(const float &dt);

	void PreCollision(const float &dt);
	void CheckCollisions(const float &dt);
	void PostCollision(const float &dt);
private:
	LPDIRECT3DDEVICE9 d3ddev; //the d3d device
	std::map<int, Drawable*> drawn_objects;
	//this is the next id we will hand out
	int next_id;
	//the collision manager for the scene
	CollisionManager collisionManager;
};
