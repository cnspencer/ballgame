// Ballgame.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Ballgame.h"

#define MAX_LOADSTRING 100

//Forward declaration of Graphics class
class Graphics;
class Game;

// Global Variables:
HINSTANCE hInst;								// current instance
HWND hWnd;										// the window
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);


int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	//seed the random number generator
	srand(static_cast<unsigned int>(time(NULL)));


	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_BALLGAME, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	//error variables
	bool error_occurred = false;
	std::wstring error_message = L"";

	//an exception may be thrown from a constructor
	//I also stuck the main loop in here in case it throws an exception
	try
	{
		game_ptr = new Game(hInst, hWnd);
		//hand control over to the game
		error_occurred = game_ptr->MainLoop();
	}
	catch(std::exception &ex)
	{
		//convert a regular char* string to a wchar_t* string
		size_t old_size = strlen(ex.what()) + 1;//for null terminator
		const size_t new_size = 256;//big enough
		wchar_t wcstring[new_size];    
		size_t convertedChars = 0;

		//the function to convert
		mbstowcs_s(&convertedChars, wcstring, old_size, ex.what(), new_size);

		//now append
		error_message.append(wcstring);
		error_occurred = true;//flag that the error happened
	}

	delete game_ptr;


	//here we should get rid of the window

	
	int returnVal = 0;//the value to return from main()
	if(error_occurred){//report error
		returnVal = 1;//for error

		//hide it so we can see our message
		ShowWindow(hWnd, SW_HIDE);
		MessageBox(NULL, error_message.c_str(), L"Error", MB_OK | MB_ICONERROR);
	}

	return returnVal;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(wcex));

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BALLGAME));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;//MAKEINTRESOURCE(IDC_BALLGAME);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   
	// Get the window size for the requested client area size
	DWORD dwStyle = (WS_EX_TOPMOST | WS_POPUP) & ~WS_THICKFRAME;
//	DWORD dwStyle = (WS_OVERLAPPEDWINDOW | WS_VISIBLE) & ~WS_THICKFRAME;
	SIZE sizeWindow;
	RECT rc;
	rc.top = rc.left = 0;
	rc.right = GetSystemMetrics(SM_CXSCREEN);///2;
	rc.bottom = GetSystemMetrics(SM_CYSCREEN);///2;
	AdjustWindowRect(&rc, dwStyle, FALSE);
	sizeWindow.cx = rc.right - rc.left;
	sizeWindow.cy = rc.bottom - rc.top;
	
	// Create window
	hWnd = CreateWindow(szWindowClass, szTitle, dwStyle, 0,
		0, sizeWindow.cx, sizeWindow.cy, NULL, NULL, hInstance, NULL);



   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}