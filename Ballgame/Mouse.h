#pragma once

class Game;
class Ball;
class Edge;



//the enum contain different mouse states
typedef enum _MouseState
{
	NORMAL,
	EDGE_STARTED,
	BALL_STARTED
} MouseState;

//the moust class itself
class Mouse
{
public:
	Mouse(HINSTANCE hInst, HWND hWnd, LPDIRECTINPUT8 din, Game *game);
	~Mouse(void);
	
	//see if the mouse has been clicked and if so, call InterpretClick
	void CheckForClick(const float &dt);

private:	
	HINSTANCE hInstance;
	HWND hWindow;

	//the current state of the mouse
	MouseState mouseState;

	Game *game;

	//a ball we are creating with the mouse
	Ball *ball;
	D3DXVECTOR2 velocity_start;

	//an edge we are creating with the mouse
	Edge *edge;
	D3DXVECTOR2 edge_start;

	//handle to the mouse
	LPDIRECTINPUTDEVICE8 dinMouse;

	//a mini buffer (??)
	DIDEVICEOBJECTDATA objData[6];

	//checks the mouse click and takes an action in the game
	void InterpretClick(bool isLeftButton, long x, long y, const float &dt);

	//randomly generates a ball
	Ball* CreateBall(float x, float y);


};
