#pragma once

//forward declaration
class Scene;

/////////////////////////////////////////////////////////////////////
// Here I'm putting the custom vertex struct and #define statement //
/////////////////////////////////////////////////////////////////////

typedef struct _CUSTOMVERTEX {
	FLOAT x, y, z, rhw;
	DWORD color;
} CUSTOMVERTEX;

#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE )


//the Graphics class, used to encapsulate all the d3d operations on the global scale
//(other classes will have handles to d3d and d3ddev to allow drawing)
class Graphics
{
public:
	Graphics(HWND hWnd);
	~Graphics(void);

	LPDIRECT3D9 get_d3d() const {return d3d;}
	LPDIRECT3DDEVICE9 get_d3ddev() const {return d3ddev;}

	void InitD3D(HWND hWnd);    // sets up and initializes Direct3D

	void UpdateFrame(const float &dt);  //updates a single frame
	bool RenderFrame(const float &dt) const;    // renders a single frame

	void CleanD3D(void);    // closes Direct3D and releases memory

	Scene* getScene(void){return scene;}
private:
	//so that we don't accidentally copy it
	//also the body is not defined, so we won't accidentally call it inside the class either
	//(or else we would get a linker error)
	Graphics(const Graphics& graphics);

	LPDIRECT3D9 d3d;    // the pointer to our Direct3D interface
	LPDIRECT3DDEVICE9 d3ddev;    // the pointer to the device class
	D3DPRESENT_PARAMETERS d3dpp;

	//background colors
	D3DXCOLOR backgroundColor;
	float red;
	float green;
	float blue;

	Scene *scene;				// the scene to be drawn

	ID3DXFont *font;
};