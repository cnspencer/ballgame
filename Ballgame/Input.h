#pragma once

#include <dinput.h>

#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

class Keyboard;
class Mouse;
class Game;

class Input
{
public:
	Input(HINSTANCE hInst, HWND hWnd, Game *game);
	~Input(void);

	//handles any input
	bool CheckInput(const float &dt);

	Keyboard* GetKeyboard(){return keyboard;}
	Mouse* GetMouse(){return mouse;}
private:
	//so that we don't accidentally copy it
	//also the body is not defined, so we won't accidentally call it inside the class either
	//(or else we would get a linker error)
	Input(const Input& input);

	HINSTANCE hInstance;
	HWND hWindow;
	LPDIRECTINPUT8 din;    // the pointer to our DirectInput interface
	Keyboard *keyboard;
	Mouse *mouse;
	Game *game;
};
