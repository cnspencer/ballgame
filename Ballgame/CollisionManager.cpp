#include "StdAfx.h"
#include "CollisionManager.h"
#include "Edge.h"
#include "Ball.h"
#include "Functions.h"
#include <sstream>
#include <typeinfo>

CollisionManager::CollisionManager(void) : 
	new_id(0)
{
}

CollisionManager::~CollisionManager(void)
{
}

//returns your id
//automatically calls SetCollisionID on the object
collision_id CollisionManager::AddEdge(Edge &edge)
{
	collision_id this_id = new_id++;
	edges.insert(std::pair<collision_id, Edge*>(this_id, &edge));
	edge.SetCollisionID(this_id);
	return this_id;
}

collision_id CollisionManager::AddBall(Ball &ball)
{
	collision_id this_id = new_id++;
	balls.insert(std::pair<collision_id, Ball*>(this_id, &ball));
	ball.SetCollisionID(this_id);
	return this_id;
}

void CollisionManager::RemoveEdge(Edge &edge)
{
	edges.erase(edge.GetCollisionID());
}

void CollisionManager::RemoveBall(Ball &ball)
{
	balls.erase(ball.GetCollisionID());
}

//checks to see if the 2 objects are colliding, and if they are

bool CollisionManager::CheckCollisionEdgeBall(collision_id edge_id, collision_id ball_id, const float &dt)
{
	Ball *ball = balls[ball_id];
	Edge *edge = edges[edge_id];
	D3DXVECTOR2 center_prop = ball->GetProposedCenter(dt);
	float radius = ball->GetRadius();
	D3DXVECTOR2 tip = edge->GetPoint(0);
	D3DXVECTOR2 tip2 = edge->GetPoint(1);

	//take and end (tip) of the edge and make a vector from there to the ball center
	D3DXVECTOR2 tip_to_center_prop(center_prop.x - tip.x, center_prop.y - tip.y);

	//draw a vector across the edge
	D3DXVECTOR2 tip_to_tip(tip2.x - tip.x, tip2.y - tip.y);
	float square_length_ttt = tip_to_tip.x * tip_to_tip.x + tip_to_tip.y * tip_to_tip.y;

	//take the projection of tip_to_center in the direction of tip_to_tip
	D3DXVECTOR2 proj(tip_to_tip);
	proj *= (tip_to_center_prop.x * tip_to_tip.x + tip_to_center_prop.y * tip_to_tip.y) / square_length_ttt;//projection complete

	//I didnt use this as I wanted to reuse square_length_ttt
//	D3DXVECTOR2 proj = bgfunc::ProjectVector(tip_to_center_prop, tip_to_tip);

	//calculate closest point to the proposed center
	D3DXVECTOR2 closest_point_prop = tip;
	closest_point_prop += proj;

	//check if the closest point lies between the endpoints
	if(!(((closest_point_prop.x >= tip.x && closest_point_prop.x <= tip2.x) || (closest_point_prop.x <= tip.x && closest_point_prop.x >= tip2.x)) &&
		((closest_point_prop.y >= tip.y && closest_point_prop.y <= tip2.y) || (closest_point_prop.y <= tip.y && closest_point_prop.y >= tip2.y))))
	{
		//if not, try for collision with the two end points
		if(CheckCollisionPointBall(tip, *ball, dt))
			return true;
		else if(CheckCollisionPointBall(tip2, *ball, dt))
			return true;
		else
			return false;
	}

	//now calculate the current closest point
	D3DXVECTOR2 center_current = ball->GetCenter();
	D3DXVECTOR2 tip_to_center_current(center_current.x - tip.x, center_current.y - tip.y);
	D3DXVECTOR2 proj2(tip_to_tip);
	proj2 *= (tip_to_center_current.x * tip_to_tip.x + tip_to_center_current.y * tip_to_tip.y) / square_length_ttt;//projection complete
	D3DXVECTOR2 closest_point_current = tip;
	closest_point_current += proj2;

	//squared distance from closest_point_prop to center of the circle
	float xdiff = closest_point_prop.x - center_prop.x;
	float ydiff = closest_point_prop.y - center_prop.y;
	float squared_distance = (xdiff * xdiff) + (ydiff * ydiff);

	//check if squared distance is greater than squared radius
	if(radius * radius > squared_distance)
	{
		//now we have a collision
		//we want to pass the current closest point, so that it bounces from its current position
		FoundCollision(*edge, *ball, closest_point_current, dt);
		return true;
	}

	//calculate the vectors pointing from closest points to centers
	D3DXVECTOR2 toward_prop = tip_to_center_prop - proj;
	D3DXVECTOR2 toward_current = tip_to_center_current - proj2;

	//now check if they are pointed away from eachother. If they are, we have a collision
	//(this only occurs if the ball is going so fast that it "jumps" over the edge and during no tick
	//does the edge get within the radius)
	if(bgfunc::DotProduct(toward_prop, toward_current) < 0.0f)
	{
		FoundCollision(*edge, *ball, closest_point_current, dt);
		return true;
	}

	return false;
}

bool CollisionManager::CheckCollisionTwoBalls(collision_id ball1, collision_id ball2, const float &dt)
{
	Ball *ball_1 = balls[ball1];
	Ball *ball_2 = balls[ball2];

	D3DXVECTOR2 center1 = ball_1->GetProposedCenter(dt);
	D3DXVECTOR2 center2 = ball_2->GetProposedCenter(dt);

	//the squared distance between the centers of the balls
	float xdiff = center1.x - center2.x;
	float ydiff = center1.y - center2.y;
	float squared_distance = (xdiff * xdiff) + (ydiff * ydiff);

	//the squared sum of the radii
	float squared_radius_sum = ball_1->GetRadius() + ball_2->GetRadius();
	squared_radius_sum *= squared_radius_sum;//square it

	//if the squared sum of the radii is larger than the squared distance, there is a collision
	if(squared_radius_sum >= squared_distance)
	{
		FoundCollision(*ball_1, *ball_2, squared_distance, dt);
		return true;
	}
	else
	{
		//check against the midpoint
		D3DXVECTOR2 midpoint1 = (balls[ball1]->GetCenter() + center1) / 2;
		xdiff = midpoint1.x - center2.x;
		ydiff = midpoint1.y - center2.y;
		squared_distance = (xdiff * xdiff) + (ydiff * ydiff);

		//now lets try again
		if(squared_radius_sum >= squared_distance)
		{
			FoundCollision(*ball_1, *ball_2, squared_distance, dt);
			return true;
		}

	}
	return false;
}

//checks for all collisions
void CollisionManager::CheckAllCollisions(const float &dt)
{
	int numberOfIterations = 2;
	float new_dt = dt/(static_cast<float>(numberOfIterations));

	std::map<collision_id, Ball*>::iterator this_ball_it;


	//lets do a few iterations of this in case we get multiple collisions in a frame
	for(int i = 0; i < numberOfIterations; ++i)
	{

		//go through all the balls
		for(this_ball_it = balls.begin();
			this_ball_it != balls.end();
			++this_ball_it)
		{
			//i commented this out because a ball taht gets checked afterwards
			//will not check for a collision with a ball with zero velocity that came before
			//because it is assumed the ones that came before already checked
			//however, I will still make a special case for null velocity balls

			
			bool isZeroVelocity = false;

			/*
			//if the velocity vector is the null vector, dont even bother
			D3DXVECTOR2 velocity = this_ball_it->second->GetVelocity();
			if(velocity.x == 0.0f && velocity.y == 0.0f)
				isZeroVelocity = true;*/

			//compare this ball to all the edges
			if(!isZeroVelocity)
			{
				std::map<collision_id, Edge*>::iterator edge_it;
				for(edge_it = edges.begin();
					edge_it != edges.end();
					++edge_it)
				{
					CheckCollisionEdgeBall(edge_it->first, this_ball_it->first, new_dt);
				}
			}

			//now check all balls after this one
			std::map<collision_id, Ball*>::iterator other_ball_it = this_ball_it;
			if(other_ball_it != balls.end())
				++other_ball_it;
			else
				continue;
			for(;other_ball_it != balls.end();
				++other_ball_it)
			{
				if(isZeroVelocity)
				{
					D3DXVECTOR2 other_vel = other_ball_it->second->GetVelocity();
				//	if(other_vel.x == 0.0f && other_vel.y == 0.0f)
				//		continue;
				}
				CheckCollisionTwoBalls(this_ball_it->first, other_ball_it->first, new_dt);
			}
		}

		//update all the objects for this iteration
		for(this_ball_it = balls.begin();
			this_ball_it != balls.end();
			++this_ball_it)
		{
			this_ball_it->second->PostCollision(new_dt);
		}
	}
/*
	// lastly force balls to be on screen
	for(this_ball_it = balls.begin();
		this_ball_it != balls.end();
		++this_ball_it)
	{
		Ball * theBall = this_ball_it->second;
		D3DXVECTOR2 center = theBall->GetCenter();
		float radius = theBall->GetRadius();
		float windowBottom = GetSystemMetrics(SM_CYSCREEN);
		float windowRight = GetSystemMetrics(SM_CXSCREEN);
		D3DXVECTOR2 velocity = theBall->GetVelocity();

		if(center.y + radius >= windowBottom)
		{
			center.y = windowBottom - radius;
			theBall->SetCenter(center);
			if(velocity.y > 0.0f)
			{
				velocity.y = -velocity.y;
				theBall->SetVelocity(velocity);
			}
		}
		else if(center.y - radius <= 0.0f)
		{
			center.y = radius;
			theBall->SetCenter(center);
			if(velocity.y < 0.0f)
			{
				velocity.y = -velocity.y;
				theBall->SetVelocity(velocity);
			}
		}

		if(center.x + radius >= windowRight)
		{
			center.x = windowBottom - radius;
			theBall->SetCenter(center);
			if(velocity.x > 0.0f)
			{
				velocity.x = -velocity.x;
				theBall->SetVelocity(velocity);
			}
		}
		else if(center.x - radius <= 0.0f)
		{
			center.x = radius;
			theBall->SetCenter(center);
			if(velocity.x < 0.0f)
			{
				velocity.x = -velocity.x;
				theBall->SetVelocity(velocity);
			}
		}
	}*/
}

void CollisionManager::FoundCollision(Ball &ball1, Ball &ball2, float squared_distance, const float &dt)
{
	//only now take a square root
	float distance = sqrt(squared_distance);

	D3DXVECTOR2 center1 = ball1.GetProposedCenter(dt);
	D3DXVECTOR2 center2 = ball2.GetProposedCenter(dt);

	//find the point where the collision occurred
	//find a point that lies on ball_1 toward ball_2
	D3DXVECTOR2 toward_2 = center2 - center1;

	//find a point that lies on ball_2 toward ball_1
	D3DXVECTOR2 toward_1 = center1 - center2;

	//take the projection of ball1's velocity in the direction of ball2
	D3DXVECTOR2 velocity1 = ball1.GetVelocity();
	D3DXVECTOR2 vel1_toward_2 = toward_2;
	vel1_toward_2 *= (velocity1.x * toward_2.x + velocity1.y * toward_2.y) / squared_distance;//projection complete
	D3DXVECTOR2 vel1_away_2 = velocity1 - vel1_toward_2;//the orthogonal component of the velocity

	//take the projection of ball2's velocity in the direction of ball1
	D3DXVECTOR2 velocity2 = ball2.GetVelocity();
	D3DXVECTOR2 vel2_toward_1 = toward_1;
	vel2_toward_1 *= (velocity2.x * toward_1.x + velocity2.y * toward_1.y) / squared_distance;//projection complete
	D3DXVECTOR2 vel2_away_1 = velocity2 - vel2_toward_1;//the orthogonal component of the velocity

	//I would also like to ensure something here: it is possible that 2 objects collide
	//and in next frame they don't have enough velocity to get away from eachother fast enough
	//to avoid another collision.  Here we see if both the "toward" vectors are pointed away from eachother
	//and if they are, don't update

	D3DXVECTOR2 relative_vel_toward_2 = velocity1 - velocity2;
//	D3DXVECTOR2 relative_vel_toward_1 = - relative_vel_toward_2;

//	bool dotProductGood = (toward_2.x * relative_vel_toward_2.x) +
//		(toward_2.y * relative_vel_toward_2.y) >= 0.0f;

	bool dotProductGood = bgfunc::DotProduct(toward_2, relative_vel_toward_2) >= 0.0f;

	//make sure dot product is negative
	if(dotProductGood)
	{
		//now swap the "towards" velocities but keep the "away" velocities
		//since they have mass, the formula is a bet complicated
		float mass1 = ball1.GetMass();
		float mass2 = ball2.GetMass();

		// the old version, which assumes that each ball has the same mass
//		D3DXVECTOR2 to_1_prime = vel1_away_2 + vel2_toward_1;
//		D3DXVECTOR2 to_2_prime = vel2_away_1 + vel1_toward_2;

		// I calculated these formulas by taking into account that both the
		// total kinetic energy and the total momentum of the system must be conserved
		// notice that if we assume the 2 balls have the same mass, we can reduce it to the
		// formulas above
		D3DXVECTOR2 to_1 = vel1_away_2 + 
			( ((2 * vel2_toward_1) + (vel1_toward_2 * ((mass1 / mass2) - 1.0f))) / 
			((mass1 / mass2) + 1.0f) ) ;

		D3DXVECTOR2 to_2 = vel2_away_1 + 
			( ((2 * vel1_toward_2) + (vel2_toward_1 * ((mass2 / mass1) - 1.0f))) / 
			((mass2 / mass1) + 1.0f) );

		//finally adjust the velocity on the balls
		ball1.SetVelocity(to_1);
		ball2.SetVelocity(to_2);
	}
}

void CollisionManager::FoundCollision(Edge &edge, Ball &ball, D3DXVECTOR2 &closest_point,  const float &dt)
{
	D3DXVECTOR2 center = ball.GetCenter();
	D3DXVECTOR2 toward = closest_point - center;
	D3DXVECTOR2 velocity = ball.GetVelocity();

	//take projection toward line
	D3DXVECTOR2 velocity_toward = toward;
	float xdiff = center.x - closest_point.x;
	float ydiff = center.y - closest_point.y;
	float squared_distance = xdiff * xdiff + ydiff * ydiff;
	velocity_toward *= (velocity.x * toward.x + velocity.y * toward.y) / squared_distance;//projection complete

//	D3DXVECTOR2 velocity_away = velocity - velocity_toward;

	ball.SetVelocity(velocity - 1.99f * velocity_toward);
}


//I'm using this for when a ball hits the tip of an edge
bool CollisionManager::CheckCollisionPointBall(D3DXVECTOR2 &point, Ball &ball, const float &dt)
{
	D3DXVECTOR2 displacement = point - ball.GetProposedCenter(dt);
	float distance = displacement.x * displacement.x + displacement.y * displacement.y;
	if(distance < ball.GetRadius() * ball.GetRadius())
	{
		FoundCollision(point, ball, dt);
		return true;
	}

	return false;
}


void CollisionManager::FoundCollision(D3DXVECTOR2 &point, Ball &ball, const float &dt)
{
	//vector from the ball center to the point
	D3DXVECTOR2 towardPoint = point - ball.GetCenter();

	//get the velocity toward the edge
	D3DXVECTOR2 velocityTowardEdge = bgfunc::ProjectVector(ball.GetVelocity(), towardPoint);

	//adjust velocity
	ball.SetVelocity(ball.GetVelocity() - 2 * velocityTowardEdge);
}