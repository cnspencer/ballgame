// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <iostream>
#include <string>
#include <time.h>
#include <math.h>
#include <exception>


// TODO: reference additional headers your program requires here

//#define SCREEN_WIDTH  800
//#define SCREEN_HEIGHT 600
#define PI 3.14159265f

#include <d3d9.h>
#include <D3dx9core.h>
#include <dinput.h>
#include <string.h>
#include <D3D9Types.h>
// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "dxtrans.lib")
